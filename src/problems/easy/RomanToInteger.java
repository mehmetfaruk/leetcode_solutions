package problems.easy;

public class RomanToInteger {

    /**

     Example 1:

     Input: s = "III"
     Output: 3
     Explanation: III = 3.

     Example 2:

     Input: s = "LVIII"
     Output: 58
     Explanation: L = 50, V= 5, III = 3.

     Example 3:

     Input: s = "MCMXCIV"
     Output: 1994
     Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
     */
    public static int romanToInt(String s) {
        int answer = 0, number = 0, prev = 0;

        for (int i = s.length() -1; i >= 0; i--) {
            switch (s.charAt(i)) {
                case 'M' -> number = 1000;
                case 'D' -> number = 500;
                case 'C' -> number = 100;
                case 'L' -> number = 50;
                case 'X' -> number = 10;
                case 'V' -> number = 5;
                case 'I' -> number = 1;
            }

            if (number < prev) {
                answer -= number;
            } else {
                answer += number;
            }

            prev = number;
        }
        System.out.println(answer);
        return answer;
    }
}

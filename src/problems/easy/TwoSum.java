package problems.easy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    public static int[] twoSum(int[] nums, int target) {
        for(int i=0; i<nums.length; i++){
            for(int j= i+1; j<nums.length; j++){
                if(nums[i] + nums[j] == target){
                    System.out.println(Arrays.toString(new int[]{i, j}));
                    return new int[] {i,j};
                }
            }
        }
        throw new IllegalStateException("No pair found");
    }

    public static int[] twoSumSecondSolution(int[] nums, int target) {
        Map<Integer,Integer> hashMap = new HashMap<>();

        for (int i=0; i<nums.length; i++){
            int remainder = target - nums[i];
            if (hashMap.containsKey(remainder)){
                System.out.println(Arrays.toString(new int[]{i, hashMap.get(remainder)}));
                return new int[]{i,hashMap.get(remainder)};
            }
            hashMap.put(nums[i],i);
        }
        throw new IllegalStateException("No pair found");
    }


}

package problems.easy;

import java.util.Stack;

public class ValidParentheses {

    public static boolean isValid(String parentheses){
        Stack<Character> stack = new Stack<>();
        char[] chars = parentheses.toCharArray();

        for (int i=0; i<chars.length; i++){
            char currentChar = chars[i];
            if (currentChar == '(' || currentChar == '{' || currentChar == '[' ){
                stack.push(currentChar);
            } else if (currentChar == ')' && !stack.isEmpty() && stack.peek() == '(') {
                stack.pop();
            }else if (currentChar == ']' && !stack.isEmpty() && stack.peek() == '[') {
                stack.pop();
            }else if (currentChar == '}' && !stack.isEmpty() && stack.peek() == '{') {
                stack.pop();
            }else
                return false;
        }

        return stack.isEmpty();
    }
}

package problems.easy;

public class ValidPalindrome {

    public static boolean isPalindrome(String s) {
        String text = s.toLowerCase();
        int startIndex = 0;
        int lastIndex = s.length()-1;

        while (startIndex <= lastIndex){
            char start = text.charAt(startIndex);
            char last = text.charAt(lastIndex);

            if (!Character.isLetterOrDigit(start)){
                startIndex++;
            } else if (!Character.isLetterOrDigit(last)) {
                lastIndex--;
            }else {
                if (Character.valueOf(start) != Character.valueOf(last)){
                    return false;
                }
                startIndex++;
                lastIndex--;
            }

        }

        return true;
    }
}
